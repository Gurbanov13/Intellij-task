package az.allstProject.get.service;

import az.allstProject.get.dto.StudentDto;
import az.allstProject.get.model.Student;
import az.allstProject.get.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository data;
    private final ModelMapper mapper;

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student map = mapper.map(dto, Student.class);
        Student save = data.save(map);
        StudentDto save2 = mapper.map(save, StudentDto.class);
        return save2;
    }

    @Override
    public StudentDto getStudentById(Long id) {
        Student byId = data.getById(id);
        StudentDto map1 = mapper.map(byId, StudentDto.class);
        return map1;

    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student = data.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Student Not Found")));
        student.setName(dto.getName());
        student.setBirthdate(dto.getBirthdate());
        student.setInstitute(dto.getInstitute());
        Student save = data.save(student);
        return mapper.map(save, StudentDto.class);

    }

    @Override
    public void deleteStudentById(Long id) {
        data.deleteById(id);
    }

    @Override
    public List<StudentDto> findAllStudent() {

        List<Student> all = data.findAll();
        List<StudentDto> km=all.stream().map(a->mapper.map(a,StudentDto.class)).collect(Collectors.toList());

        return km;
    }


}
