package ingress.ms9.controller;

import ingress.ms9.dto.StudentDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping

    public StudentDto sayHello(@RequestBody StudentDto dto){
        return dto;
    }
}
