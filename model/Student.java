package ingress.ms9.model;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Data
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    private String name;
    private String institute;
    private LocalDate birthdate;
}
